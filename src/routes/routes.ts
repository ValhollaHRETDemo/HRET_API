

import { getUsers, getUsersById, getUserDetails, getUserLoginDetails, saveUsers, deleteUsers, getUsersByUserId, login, verification, generateOTP, changePassword, resetPassword } from "../controllers/userController";
import { getReports, downloadReportFile } from "../controllers/reportController";
import { getstates, getAllData, getstateById, savestate, deletestate } from "../controllers/statesController";
import { gethealthSystems, gethealthSystemsById, gethealthSystemsByStateId, savehealthSystems, deletehealthSystems } from "../controllers/healthSystemsController";
import { getHospitalInformation, getHospitalInformationById, getHospitalInformationByStateId, saveHospitalInformation, deleteHospitalInformation } from "../controllers/hospitalInformationController";
import { gethealthSystemAndHospital, gethealthSystemAndHospitalById, getHealthSystemHospitalByHealthSystemId, savehealthSystemAndHospital, deletehealthSystemAndHospital } from "../controllers/healthSystemAndHospitalController";
import { gethospitalUnits, gethospitalUnitsById, getDataByHospitalId, savehospitalUnits, deletehospitalUnits } from "../controllers/hospitalUnitsController";
import { getMultifactorAuthentication, getMultifactorAuthenticationById, saveMultifactorAuthentication, deleteMultifactorAuthentication } from "../controllers/multifactorAuthentication";
import { getRoles, getRolesById, saveRoles, deleteRoles } from "../controllers/rolesController";
import { getUnits, getUnitsById, getUnitsByHospId, saveUnits, deleteUnits } from "../controllers/unitsController";
import { getUserAndRoles, getUserAndRolesById, getUserAndRolesByUserId, saveUserAndRoles, deleteUserAndRoles } from "../controllers/userAndRolesController";
import { deleteDomain, getDomain, getDomainById, saveDomain, validateDomain } from "../controllers/domainController";
import { getReportsFilter } from "../controllers/reportsFilterController";

export const AppRoutes = [

    //auth
    {
        path: "/login",
        method: "post",
        action: login
    },
    {
        path: "/verification",
        method: "post",
        action: verification
    },
    {
        path: "/generateOTP",
        method: "post",
        action: generateOTP
    },
    {
        path: "/changePassword",
        method: "post",
        action: changePassword
    },
    {
        path: "/resetPassword",
        method: "post",
        action: resetPassword
    },

    //users
    {
        path: "/users",
        method: "get",
        action: getUsers
    },
    {
        path: "/users/:id",
        method: "get",
        action: getUsersById
    },
    {
        path: "/users/getUserDetails/:id",
        method: "get",
        action: getUserDetails
    },
    {
        path: "/users/getUserLoginDetails",
        method: "post",
        action: getUserLoginDetails
    },
    {
        path: "/users",
        method: "post",
        action: saveUsers
    },
    {
        path: "/users/delete",
        method: "post",
        action: deleteUsers
    },
    {
        path: "/users/getUsersByUserId/:id",
        method: "get",
        action: getUsersByUserId
    },

    {
        path: "/users/changepassword",
        method: "post",
        action: changePassword
    },

    //states
    {
        path: "/states",
        method: "get",
        action: getstates
    },
    {
        path: "/states/allStates",
        method: "get",
        action: getAllData
    },
    {
        path: "/states/:id",
        method: "get",
        action: getstateById
    },
    {
        path: "/states",
        method: "post",
        action: savestate
    },
    {
        path: "/states/delete",
        method: "post",
        action: deletestate
    },

    // Health Systems
    {
        path: "/healthSystems",
        method: "get",
        action: gethealthSystems
    },
    {
        path: "/healthSystems/:id",
        method: "get",
        action: gethealthSystemsById
    },
    {
        path: "/healthSystems/gethealthSystemsByStateId/:id",
        method: "get",
        action: gethealthSystemsByStateId
    },
    {
        path: "/healthSystems",
        method: "post",
        action: savehealthSystems
    },
    {
        path: "/healthSystems/delete",
        method: "post",
        action: deletehealthSystems
    },

    // Hospital Information Systems
    {
        path: "/hospitalInformation",
        method: "get",
        action: getHospitalInformation
    },
    {
        path: "/hospitalInformation/:id",
        method: "get",
        action: getHospitalInformationById
    },
    {
        path: "/hospitalInformation/getHospitalInformationByStateId/:id",
        method: "get",
        action: getHospitalInformationByStateId
    },
    {
        path: "/hospitalInformation",
        method: "post",
        action: saveHospitalInformation
    },
    {
        path: "/hospitalInformation/delete",
        method: "post",
        action: deleteHospitalInformation
    },

    //healthSystemsAndHospital
    {
        path: "/healthSystemsAndHospital",
        method: "get",
        action: gethealthSystemAndHospital
    },
    {
        path: "/healthSystemsAndHospital/:id",
        method: "get",
        action: gethealthSystemAndHospitalById
    },
    {
        path: "/healthSystemsAndHospital/HealthSystemHospitalByHealthSystemId/:id",
        method: "get",
        action: getHealthSystemHospitalByHealthSystemId
    },
    {
        path: "/healthSystemsAndHospital",
        method: "post",
        action: savehealthSystemAndHospital
    },
    {
        path: "/healthSystemsAndHospital/delete/:id",
        method: "post",
        action: deletehealthSystemAndHospital
    },
    //HospitalUnits
    {
        path: "/hospitalUnits",
        method: "get",
        action: gethospitalUnits
    },
    {
        path: "/hospitalUnits/:id",
        method: "get",
        action: gethospitalUnitsById
    },
    {
        path: "/hospitalUnits/hospitalUnitsByHospital/:id",
        method: "get",
        action: getDataByHospitalId
    },
    {
        path: "/hospitalUnits",
        method: "post",
        action: savehospitalUnits
    },
    {
        path: "/hospitalUnits/delete/:id",
        method: "post",
        action: deletehospitalUnits
    },

    //multifactor Authentication
    {
        path: "/multifactorAuthentication",
        method: "get",
        action: getMultifactorAuthentication
    },
    {
        path: "/multifactorAuthentication/:id",
        method: "get",
        action: getMultifactorAuthenticationById
    },
    {
        path: "/multifactorAuthentication",
        method: "post",
        action: saveMultifactorAuthentication
    },
    {
        path: "/multifactorAuthentication/delete/:id",
        method: "post",
        action: deleteMultifactorAuthentication
    },

    //roles
    {
        path: "/roles",
        method: "get",
        action: getRoles
    },
    {
        path: "/roles/:id",
        method: "get",
        action: getRolesById
    },
    {
        path: "/roles",
        method: "post",
        action: saveRoles
    },
    {
        path: "/roles/delete/:id",
        method: "post",
        action: deleteRoles
    },

    //units
    {
        path: "/units",
        method: "get",
        action: getUnits
    },
    {
        path: "/units/:id",
        method: "get",
        action: getUnitsById
    },
    {
        path: "/units/getUnitsByHospId/:id",
        method: "get",
        action: getUnitsByHospId
    },
    {
        path: "/units",
        method: "post",
        action: saveUnits
    },
    {
        path: "/units/delete",
        method: "post",
        action: deleteUnits
    },

    //user And Roles
    {
        path: "/userAndRoles",
        method: "get",
        action: getUserAndRoles
    },
    {
        path: "/userAndRoles/:id",
        method: "get",
        action: getUserAndRolesById
    },
    {
        path: "/userAndRoles/userAndRolesByUserId/:id",
        method: "get",
        action: getUserAndRolesByUserId
    },
    {
        path: "/userAndRoles",
        method: "post",
        action: saveUserAndRoles
    },
    {
        path: "/userAndRoles/delete/:id",
        method: "post",
        action: deleteUserAndRoles
    },
    //reports
    {
        path: "/reports",
        method: "post",
        action: getReports
    },
    {
        path: "/reports/exporttoexcel",
        method: "post",
        action: downloadReportFile
    },
    {
        path: "/reports/getFilterData/:id",
        method: "get",
        action: getReportsFilter
    },
    //domain
    {
        path: "/domain",
        method: "get",
        action: getDomain
    },
    {
        path: "/domain/:id",
        method: "get",
        action: getDomainById
    },
    {
        path: "/domain",
        method: "post",
        action: saveDomain
    },
    {
        path: "/domain/delete",
        method: "post",
        action: deleteDomain
    },
    {
        path: "/domain/validatedomain",
        method: "post",
        action: validateDomain
    }
];