const express = require("express");
const path = require("path");
const request = require("request");
const _ = require("underscore");
const jsonwebtoken = require("jsonwebtoken");
const passportJwt = require("passport-jwt");
const passport = require("passport");
const  UserRepository = require("../../src/Repository/userRepository");
const AppConfig = require("../config");
const ExtractJwt = passportJwt.ExtractJwt;
const JwtStrategy = passportJwt.Strategy;

const jwtOptions = {
    secretOrKey: "",
    session: false
};

jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
jwtOptions.secretOrKey = AppConfig.tokenSecret;

const strategy = new JwtStrategy(jwtOptions, async (jwt_payload, next) => {

    const user = await userRepository.getDataById(jwt_payload.userId);
    _.extend(user, jwt_payload)
    if (user) {
        next(undefined, user);
    } else {
        next(undefined, false);
    }
});

module.exports = strategy;