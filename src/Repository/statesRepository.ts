
import { getManager, EntityRepository, Repository } from "typeorm";
import { states } from "../entity/states";
import * as _ from "underscore";

@EntityRepository()
export class StateRepository extends Repository<states> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(states);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ is_active: "1", is_hret: "1" });

        return _.sortBy(data, 'state_name');
    }


    //Get all data from the database
    async getAllStatesData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ is_active: "1", is_hret: "0" });

        return _.sortBy(data, 'state_name');;
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id, { is_active: "1" });

        return data;
    }

    // save data into database by id
    async saveData(data: states) {

        data.is_hret = "1";

        //creates the users into the repository instance 
        const entity = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(entity);
    }

    //delete the data by id
    async deleteData(data: states) {

        //soft deleting the data
        data.is_hret = "0";

        //creates the users into the repository instance
        //const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(data);
    }
}

