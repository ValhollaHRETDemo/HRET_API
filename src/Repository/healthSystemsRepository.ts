
import { getManager, EntityRepository, Repository } from "typeorm";
import { health_systems } from "../entity/health_systems";
import * as _ from "underscore";

@EntityRepository()
export class healthSystemsRepository extends Repository<health_systems> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(health_systems);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        // const data = await this._repository.find({ relations: ["states"] });
        const data = await this._repository.find({ is_active: "1", relations: ["state_"] });

        return _.sortBy(data, 'health_system_name');
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    //Get Specific data from database by id
    async getDataByStateId(stateId: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.find({ is_active: "1", state_id: stateId, relations: ["state_"] });

        return data;
    }

    // save data into database by id
    async saveData(data: health_systems) {
        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        await this._repository.save(state);

        return this._repository.findOne({ health_system_name: data.health_system_name });
    }

    //delete the data by id
    async deleteData(data: health_systems) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const healthSystem = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(healthSystem);;
    }
}