
import { getManager, EntityRepository, Repository } from "typeorm";
import { rpt_gen_log } from "../entity/rpt_gen_log";

@EntityRepository()
export class ReportStatisticsRepository extends Repository<rpt_gen_log> {

    _repository: any;
    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(rpt_gen_log);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ is_active: "1", is_hret: "1" });

        return data;
    }

    // save data into database by id
    async saveData(data: rpt_gen_log) {

        //creates the users into the repository instance 
        const entity = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(entity);
    }
}

