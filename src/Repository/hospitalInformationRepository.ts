
import { getManager, EntityRepository, Repository } from "typeorm";
import { hosp_information } from "../entity/hosp_information";
import * as _ from "underscore";

@EntityRepository()
export class hospitalInformationRepository extends Repository<hosp_information> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(hosp_information);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ is_active: "1", relations: ["state_"] });

        return _.sortBy(data, 'hospital_name');
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id, { is_active: "1", relations: ["state_"] });

        return data;
    }

    //Get Specific data from database by id
    async getDataByStateId(stateId: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.find({ is_active: "1", state_id: stateId });

        return data;
    }

    // save data into database by id
    async saveData(data: hosp_information) {

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        await this._repository.save(state);

        return this._repository.findOne({ hospital_name: data.hospital_name });
    }

    //delete the data by id
    async deleteData(data: hosp_information) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}