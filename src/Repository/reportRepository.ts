import { getConnectionManager, AdvancedConsoleLogger } from "typeorm";
import { hret_report } from "../entity/hret_report"
import { NationalLevelQuery } from "./ReportQueries/nationalLevelQuery";
import { HospitalLevelQuery } from "./ReportQueries/hospitalLevelQuery";
import { StateLevelQuery } from "./ReportQueries/stateLevelQuery";
import { ReportStatisticsRepository } from "./reportStatisticsRepository";
import { rpt_gen_log } from "../entity/rpt_gen_log";

export class ReportsRepository {
    _connection: any;
    _reportLogRepository: ReportStatisticsRepository;
    constructor() {
        this._connection = getConnectionManager().get("default");
        this._reportLogRepository = new ReportStatisticsRepository();
    }

    async generateReport(reportParameters) {

        var reportLog = new rpt_gen_log();
        reportLog.rpt_parameter = JSON.stringify(reportParameters);
        reportLog.measure_id = reportParameters.measure;
        reportLog.user_id = reportParameters.userId;
        reportLog.rpt_start_time = new Date();

        try {
            var query = query = this.createQuery(reportParameters, query);
            var data = await query.execute(this._connection, reportParameters);
            console.log(data)
            var hretReport: hret_report = await query.generateReport(
                { rows: data, reportParameters: reportParameters });

            reportLog.rpt_end_time = new Date();
            reportLog.rpt_status = "1";
            this._reportLogRepository.saveData(reportLog);

            return hretReport;
        }
        catch (ex) {

            reportLog.rpt_error = JSON.stringify(ex)
            reportLog.rpt_end_time = new Date();
            reportLog.rpt_status = "0";
            this._reportLogRepository.saveData(reportLog);
        }
    }

    private createQuery(reportParameters: any, query: any) {
        if (reportParameters.reportLevel == "National") {
            query = new NationalLevelQuery();
        }
        if (reportParameters.reportLevel == "State") {
            query = new StateLevelQuery();
        }
        if (reportParameters.reportLevel == "Hospital") {
            query = new HospitalLevelQuery();
        }
        return query;
    }
}
