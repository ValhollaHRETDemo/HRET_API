
import { getManager, EntityRepository, Repository } from "typeorm";
import { users } from "../entity/users";
import { Request } from "express";
import * as randomGenerator from "random-string";
import * as moment from 'moment';

var salt = "sddsj4o3j4lljq3434n3nq.ewqe.,43";
var AppConfig = require("../config")
const md5 = require("md5");
const UUID = require("uuid/v4");
const JWT = require("jsonwebtoken");
import * as _ from "underscore";
@EntityRepository()
export class UserRepository extends Repository<users> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(users);
    }

    async getUserDetails(email) {
        // Instanciates repository and gets all data
        const data = await this._repository.findOne({ is_active: '1', email_id: email });
        return data;
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        // const data = await this._repository.find({ relations: ["states","user_roles","mf_authentications"]});
        const data = await this._repository.find({ is_active: '1' });
        return _.sortBy(data, 'first_name');
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        // Modified bec of state relation issue
        //var data = await this._repository.findOne(id,{ relations: ["states","user_roles","mf_authentications"] });
        var data = await this._repository.findOne(id);

        //return data
        return data;
    }

    //Get Specific data from database by id
    async getUserLoginDetails(emailId: string, password: string) {

        // Instanciates repository and gets specific data
        // var data = await this._repository.findOne("", { relations: ["states", "user_roles", "mf_authentications"] });
        const data = await this._repository.find({ email_id: emailId, user_pwd: password, is_active: '1' });

        //return data
        return data;
    }

    // save data into database by id
    async saveData(data: users) {

        //creates the users into the repository instance
        const user = await this._repository.create(data);
        if (!user.user_id) {
            user.email_id = user.email_id.toLocaleLowerCase();
            user.user_pwd = this.generateRandomPassword();
            user.password_expiry_date = this.passwordExpiryDate();
            user.is_auto_password = '1';
            user.is_locked = '0';
            user.failure_count = '0';
        }
        //save (or) insert the data into the db
        await this._repository.save(user);

        return this._repository.findOne({ email_id: data.email_id });
    }

    //resetPassword
    async resetPassword(email_id) {
        //get the user details through email id
        const user_details = await this._repository.findOne({ email_id: email_id, is_active: '1' });

        user_details.user_pwd = this.generateRandomPassword();
        user_details.is_auto_password = '1';
        user_details.is_locked = '0';
        //creates the users into the repository instance
        const user = await this._repository.create(user_details);

        //save (or) insert the data into the db
        return this._repository.save(user);

    }

    // change password
    async changePassword(user_id, new_pwd) {

        const user_details = await this._repository.findOne({ user_id: user_id });

        if (user_details) {

            //sets the is_auto_password field to 0
            user_details.is_auto_password = '0';

            //sets the new_pwd to user_pwd field
            user_details.user_pwd = new_pwd;

            //sets the modified_on to current time
            user_details.modified_on = moment();

            //sets the modified_by to user_id
            user_details.modified_by = user_id;

            //creates the users into the repository instance
            const user = await this._repository.create(user_details);

            //save (or) insert the data into the db
            return this._repository.save(user);
        }
    }

    //delete the data by id
    async deleteData(data: users) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const user = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(user);
    }


    // UserSchema.statics.computePassword = function (pass) {
    async computePassword(pass) {
        return md5(pass + salt);
    }

    async generateToken(user_id, role_details, user_roles, state_details, hospital_details) {
        const user = await this.getDataById(user_id)
        if (user != undefined) {
            const payload = {
                user_id: user.user_id,
                email_id: user.email_id,
                first_name: user.first_name,
                last_name: user.last_name,
                phone: user.phone,
                is_locked: user.is_locked,
                failure_count: user.failure_count,
                is_auto_password: user.is_auto_password,
                role_id: user_roles.role_id,
                role_name: role_details.role_name,
                role_rank: role_details.role_rank,
                state_id: user_roles.state_id,
                state_code: (state_details) ? state_details.state_code : null,
                state_name: (state_details) ? state_details.state_name : null,
                hs_id: user_roles.hs_id,
                hospital_id: user_roles.hospital_id,
                hospital_code: (hospital_details) ? hospital_details.hiin_id : null,
                hospital_name: (hospital_details) ? hospital_details.hospital_name : null,
                hosp_peer_group_id: (hospital_details) ? hospital_details.hosp_peer_group_id : null,
                bed_range_id: (hospital_details) ? hospital_details.bed_range_id : null,
                rural_id: (hospital_details) ? hospital_details.rural_id : null,
                unit_id: user_roles.unit_id
            };
            const options = {
                issuer: "HRET",
                audience: "HRETAPP",
                jwtid: UUID(),
                expiresIn: 30 * 24 * 60 * 60
            };

            const token = JWT.sign(payload, AppConfig.tokenSecret, options);
            return token;
        }
        else {
            throw "Login Failed";
        }

    }

    generateRandomCode(stringLength: number, allowNumeric: boolean, allowLetters: boolean, allowSpecialCharacters: boolean) {
        var mfa = randomGenerator({
            length: stringLength,
            numeric: allowNumeric,
            letters: allowLetters,
            special: allowSpecialCharacters
        });
        return mfa;
    }

    generateRandomMFA() {
        var randomMFA = this.generateRandomCode(6, true, false, false);
        randomMFA = (parseInt(randomMFA) > 100000) ? parseInt(randomMFA) : (parseInt(randomMFA) + 100000);
        return randomMFA.toString();
    }

    generateRandomPassword() {
        return this.generateRandomCode(10, true, true, false);
    }

    passwordExpiryDate() {
        let passwordExpiryDate = moment().add(60, 'days').toDate();
        return passwordExpiryDate;
    }
}