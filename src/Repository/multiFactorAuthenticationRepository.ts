
import { getManager, EntityRepository, Repository } from "typeorm";
import { mf_authentication } from "../entity/mf_authentication";

@EntityRepository()
export class multifactorAuthenticationRepository extends Repository<mf_authentication> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(mf_authentication);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find();

        return data;
    }

    //Get Specific data from database by user_id
    async getDataByUserId(user_id) {
        // Instanciates repository and gets all data
        const data = await this._repository.findOne({ is_success: '0', user_id: user_id });
        return data;
    }

    async getLatestAuthDataByUserId(user_id) {
        const data = await getManager()
            .createQueryBuilder(mf_authentication, "mf_authentication")
            .where("mf_authentication.user_id = :user_id", { user_id: user_id })
            .andWhere("mf_authentication.is_success = :is_success", { is_success: '0' })
            .andWhere("mf_authentication.is_active = :is_active", { is_active: '1' })
            .orderBy("mf_authentication.expiry_time", "DESC")
            .getOne();
        return data;
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    // save data into database by id
    async saveData(data: mf_authentication) {

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }

    //delete the data by id
    async deleteData(data: mf_authentication) {

        //soft deleting the data
        //data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}