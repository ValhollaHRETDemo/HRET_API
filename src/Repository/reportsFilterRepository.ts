
import { getManager, EntityRepository, Repository } from "typeorm";
import { bed_range } from "../entity/bed_range";
import { hosp_group } from "../entity/hosp_group";
import { sub_measures } from "../entity/sub_measures";
import { rural_values } from "../entity/rural_values";
import { random_numbers } from "../entity/random_numbers";
import { comparables } from "../entity/comparables";

import * as _ from "underscore";

@EntityRepository()
export class ReportsFilterRepository extends Repository<bed_range> {

    _bedRangeRepository: any;
    _hospitalGroupsRepository: any;
    _subMeasuresRepository: any;
    _ruralRepository: any;
    _randomNumbersRepository: any;
    _comparablesRepository: any;

    constructor() {
        super();
        //repository instance
        this._bedRangeRepository = getManager().getRepository(bed_range);
        this._hospitalGroupsRepository = getManager().getRepository(hosp_group);
        this._subMeasuresRepository = getManager().getRepository(sub_measures);
        this._ruralRepository = getManager().getRepository(rural_values);
        this._randomNumbersRepository = getManager().getRepository(random_numbers);
        this._comparablesRepository = getManager().getRepository(comparables);

    }

    //Get all data from the database
    async getFilterData(measureId, reportLevel, hospital) {

        // Instanciates repository and gets all data
        const bedRangeData = (await this._bedRangeRepository.find({ is_active: "1" })).map(element => { return { id: element.bed_range_id, name: element.bed_range, sort_by: element.sort_by } });
        const hospitalGroupsData = (await this._hospitalGroupsRepository.find({ is_active: "1" })).map(element => { return { id: element.hosp_group_id, name: element.hosp_group } });
        const subMeasuresData: any = _.sortBy((await this._subMeasuresRepository.find({ is_active: "1", measure_id: measureId })).map(element => { return { id: element.sub_measure_id, name: (element.measure_code + " - " + element.measure_description), submeasureCode: element.measure_code } }), 'submeasureCode');
        const randomNumbers = (await this._randomNumbersRepository.find()).map(element => { return { id: element.random_id, name: element.random_number } });
        const ruralValues = (await this._ruralRepository.find()).map(element => { return { id: element.rural_value, name: element.display_name } });
        const comparables = (await this._comparablesRepository.find({ is_active: "1" })).map(element => { return { id: element.comparable_id, name: element.comparable_name, comparable_value: element.comparable_value, sort_order: element.sort_order } });

        var filterResult = [];
        filterResult.push({
            id: "bedRange",
            name: "Bed Range",
            data: _.sortBy(bedRangeData, 'sort_by'),
            type: "DropDown",
            selectedId: null,
            position: 2,
            defaultValue: null,
            forReportLevels: ["National", "State", "Hospital"]
        });
        filterResult.push({
            id: "hospitalGroupsData",
            name: "Hospital Groups",
            data: _.sortBy(hospitalGroupsData, 'name'),
            type: "DropDown",
            selectedId: null,
            position: 3,
            defaultValue: null,
            forReportLevels: ["National", "State", "Hospital"]
        })

        filterResult.push({
            id: "measureCodes",
            name: "Sub Measures",
            data: subMeasuresData,
            type: "DropDown",
            selectedId: subMeasuresData[0].id,
            position: 1,
            defaultValue: subMeasuresData[0].id,
            forReportLevels: ["National", "State", "Hospital"]
        });

        filterResult.push({
            id: "ruralValues",
            name: "Rural",
            data: _.sortBy(ruralValues, 'id'),
            type: "DropDown",
            selectedId: null,
            position: 4,
            defaultValue: null,
            forReportLevels: ["National", "State", "Hospital"]
        });

        filterResult.push({
            id: "randomNumbers",
            name: "Random Numbers",
            data: _.sortBy(randomNumbers, 'name'),
            type: "DropDown",
            selectedId: null,
            position: 6,
            defaultValue: null,
            forReportLevels: ["Hospital"]
        })
        filterResult.push({
            id: "comparables",
            name: "Comparables",
            data: _.sortBy(comparables, 'sort_order'),
            type: "DropDown",
            selectedId: null,
            position: 5,
            defaultValue: null,
            forReportLevels: ["Hospital"]
        });

        return filterResult;
    }
}