import { ReportQuery } from "./ReportQuery";
import * as _ from "underscore"
import { hret_report } from "../../entity/hret_report";

export class NationalLevelQuery extends ReportQuery {

    private nationalAverageQuery: string;
    private allStatesQuery: string;

    constructor() {

        super();

        this.nationalAverageQuery = `SELECT 
        _out_measure_code  as measure_code,
        'National Average' as state_code,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp
        from usp_get_national_hret_report($1,$2,$3,$4,$5,$6)`

        this.allStatesQuery = `SELECT 
        _out_measure_code as measure_code,
        _out_state_code as state_code,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp
        from usp_get_state_hret_report($1,$2,$3,$4,$5,$6,null)`
    }


    async execute(connection, reportParameters) {
        var nationalAverage = connection.query(this.nationalAverageQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange]);

        var allStatesAverages = connection.query(this.allStatesQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange]);

        nationalAverage.catch(ex => { throw ex })
        allStatesAverages.catch(ex => { throw ex })

        var [nationalAverageRow, allStatesAveragesRows] = await Promise.all([nationalAverage, allStatesAverages])
        return nationalAverageRow.concat(allStatesAveragesRows);
    }

    async generateReport(reportData) {

        var reportRows = [];
        var valueColumn = "rate";
        var timeWindowColumns = [];
        var propertyColumns = [];
        var keyColumns = [];

        keyColumns.push({
            id: "state_code",
            name: "State",
            index: 0
        })

        var timeWindowColumnStartIndex = 1;

        timeWindowColumns.push({ id: "baseline", name: "Baseline", index: timeWindowColumnStartIndex })

        timeWindowColumnStartIndex++;
        this.generateTimeWindowSeries(reportData.reportParameters.startDate, reportData.reportParameters.endDate)
            .forEach((value, index) => timeWindowColumns.push({ id: value, name: value, index: timeWindowColumnStartIndex + index }));


        var rowsGroupedByState = _.chain(reportData.rows)
            .map((row) => {
                if (row["_out_time_frame"] == "Baseline") row["_out_year_month"] = "Baseline"
                return row;
            }).groupBy(row => row["_out_state_code"]).value();


        _.chain(rowsGroupedByState).allKeys()
            .each((state) => {
                var row = [];
                timeWindowColumns.forEach(column => {
                    var rowValue = _.find(rowsGroupedByState[state], row => row["_out_year_month"] === column.name);
                    if (rowValue) row.push(rowValue["_out_rate"])
                    else row.push(0)
                })
                reportRows.push([state, ...row]);
            });

        var hretReport: hret_report = {
            rows: reportRows,
            key_columns: keyColumns,
            property_columns: propertyColumns,
            time_window_columns: timeWindowColumns,
            keys: Object.keys(rowsGroupedByState),
            report_level: "National",
            report_generated_time: new Date(),
            report_name: "HRET-National"
        };

        return hretReport;
    }

}
