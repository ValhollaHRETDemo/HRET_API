import { hret_report } from "../../entity/hret_report";
import * as xl from 'excel4node';
import * as _ from "underscore"

export class ReportFileGenerator {

    async generateFile(hretReport: hret_report, userFilterValues) {
        var wb = new xl.Workbook();
        var dataSheet = wb.addWorksheet('ReportData');
        var infoSheet = wb.addWorksheet('ReportInfo');
        var headerStyle = wb.createStyle({
            font: {
                bold: true,
            }

        });

        infoSheet.cell(1, 1).string("Report Level").style(headerStyle);
        infoSheet.cell(2, 1).string("Measure").style(headerStyle);
        infoSheet.cell(3, 1).string("Sub Measure").style(headerStyle);
        infoSheet.cell(4, 1).string("Bed Range").style(headerStyle);
        infoSheet.cell(5, 1).string("Hospital Groups ").style(headerStyle);
        infoSheet.cell(6, 1).string("Is Rural").style(headerStyle);
        infoSheet.cell(7, 1).string("Comparables").style(headerStyle);
        infoSheet.cell(8, 1).string("Number of Random Hospitals for Comparison").style(headerStyle);
        infoSheet.cell(9, 1).string("Report Generated At").style(headerStyle);

        infoSheet.cell(1, 2).string(hretReport.report_level);
        infoSheet.cell(2, 2).string(userFilterValues.measure);
        infoSheet.cell(3, 2).string(userFilterValues.sub_measure);
        infoSheet.cell(4, 2).string(userFilterValues.bed_range);
        infoSheet.cell(5, 2).string(userFilterValues.hospital_groups);
        infoSheet.cell(6, 2).string(userFilterValues.is_rural);
        infoSheet.cell(7, 2).string(userFilterValues.comparables);
        infoSheet.cell(8, 2).string(userFilterValues.random);
        infoSheet.cell(9, 2).string(hretReport.report_generated_time.toLocaleDateString());

        var headerLine =
            [..._.map(hretReport.key_columns, c => c.name),
            ..._.map(hretReport.property_columns, c => c.name),
            ...(_.map(hretReport.time_window_columns, c => c.name))];

        headerLine.forEach((name, index) => dataSheet.cell(1, index + 1).string(name).style(headerStyle));

        hretReport.rows.forEach((row, rowIndex) => {
            row.forEach((value, columnIndex) => dataSheet.cell(rowIndex + 2, columnIndex + 1).string(value))
        })
        return wb;
    }
}