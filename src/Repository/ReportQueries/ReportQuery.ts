import { hret_report } from "../../entity/hret_report";
import * as _ from "underscore"
import * as moment from "moment";

export class ReportQuery {


    constructor() {


    }

    execute(connection, reportParameters) {
        return undefined;
    }

    async generateReport(data): Promise<hret_report> {

        return undefined;
    }

    generateTimeWindowSeries(startDate, endDate) {

        console.log(endDate);

        if (startDate == undefined) {
            endDate = moment(startDate).add(365, 'days');
        }

        var dateStart = moment(startDate);
        var dateEnd = moment(endDate);

        var timeValues = [];
        while (dateEnd > dateStart || dateStart.format('M') === dateEnd.format('M')) {
            timeValues.push(dateStart.format('YYYY-MM'));
            dateStart.add(1, 'month');
        }
        return timeValues;
    }
} 