import { ReportQuery } from "./ReportQuery";
import { hret_report } from "../../entity/hret_report";
import * as _ from "underscore"

export class StateLevelQuery extends ReportQuery {

    protected nationalAverageQuery: string;
    protected stateAverageQuery: string;
    private allHospitalsQuery: string;

    constructor() {
        super()
        this.nationalAverageQuery = `SELECT
         _out_measure_code as measure_code,
        'National Average' as state_code,
        '' AS hiin_id ,
        '' AS hospital_name,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp  
        from usp_get_national_hret_report($1,$2,$3,$4,$5,$6)`

        this.stateAverageQuery = `SELECT 
        _out_measure_code as measure_code, 
        'State Average' AS state_code, 
        '' AS hiin_id ,
        '' AS hospital_name,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp 
        from usp_get_state_hret_report($1,$2,$3,$4,$5,$6,$7)`

        this.allHospitalsQuery = `SELECT
        _out_measure_code as measure_code,
        _out_state_code as state_code,
        _out_hiin_id as hiin_id,
        _out_hospital_name as hospital_name,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp 
        from usp_get_hospital_hret_report($1, $2, $3, $4, $5, $6, $7, null, null, null,null)`

    }

    async execute(connection, reportParameters) {
        var nationalAverage = connection.query(this.nationalAverageQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange]);

        var stateAverage = connection.query(this.stateAverageQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange,
            reportParameters.stateCode]);

        var allHospitals = connection.query(this.allHospitalsQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange,
            reportParameters.stateCode]);

        nationalAverage.catch(ex => { throw ex })
        stateAverage.catch(ex => { throw ex })
        allHospitals.catch(ex => { throw ex })

        var [nationalAverageRow, stateAverageRow, allHospitalRows] = await Promise.all([nationalAverage, stateAverage, allHospitals])
        return nationalAverageRow.concat(stateAverageRow).concat(allHospitalRows);
    }





    async generateReport(reportData) {
        console.log(" Hosp : " + reportData);
        var reportRows = [];

        var timeWindowColumns = [];
        var propertyColumns = [];
        var keyColumns = [];

        keyColumns.push({
            id: "hiin_id",
            name: "Hospital ID",
            index: 0,
            isHidden: true
        })

        propertyColumns.push({
            id: "hospital_name",
            name: "Hospital",
            index: 0
        })

        //first column is hospital
        var timeWindowColumnStartIndex = 2;
        var rowsGroupedByHospital = _.chain(reportData.rows).map((row) => {
            if (row["_out_time_frame"] == "Baseline") row["_out_year_month"] = "Baseline"
            return row;
        })
            .groupBy(row => row["_out_hiin_id"] + "," + row["_out_hospital_name"]).value();

        timeWindowColumns.push({ id: "Baseline", name: "Baseline", index: timeWindowColumnStartIndex })

        timeWindowColumnStartIndex++;

        this.generateTimeWindowSeries(reportData.reportParameters.startDate, reportData.reportParameters.endDate)
            .forEach((value, index) => timeWindowColumns.push({ id: value, name: value, index: timeWindowColumnStartIndex + index }));

        _.chain(rowsGroupedByHospital).allKeys().each((hospital) => {

            var row = [];
            timeWindowColumns.forEach(column => {
                var rowValue = _.find(rowsGroupedByHospital[hospital], row => row["_out_year_month"] === column.name);
                if (rowValue) row.push(rowValue["_out_rate"])
                else row.push(0)
            })
            reportRows.push([...hospital.split(","), ...row]);
        });

        var hretReport: hret_report = {
            rows: reportRows,
            key_columns: keyColumns,
            property_columns: propertyColumns,
            time_window_columns: timeWindowColumns,
            keys: Object.keys(rowsGroupedByHospital).map(key => key.split(",")[1]),
            report_level: "State",
            report_generated_time: new Date(),
            report_name: "HRET-State-" + reportData.reportParameters.stateCode
        };

        return hretReport;
    }

}
