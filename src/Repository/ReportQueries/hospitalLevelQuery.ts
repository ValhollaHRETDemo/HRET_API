import { ReportQuery } from "./ReportQuery";
import { hret_report } from "../../entity/hret_report";
import * as _ from "underscore"
import { StateLevelQuery } from "./stateLevelQuery";
import { hospitalInformationRepository } from "../hospitalInformationRepository";

export class HospitalLevelQuery extends StateLevelQuery {

    protected hospitalQuery: string;
    protected comparablesQuery: string;

    constructor() {
        super()

        this.hospitalQuery = `SELECT
        _out_measure_code as measure_code,
        _out_state_code as state_code,
        _out_hiin_id as hiin_id,
        _out_hospital_name as hospital_name,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp
        from usp_get_hospital_hret_report($1,$2,$3,$4,$5,$6,$7,$8,null,null)`


        this.comparablesQuery = `SELECT 
        _out_measure_code as measure_code,
        _out_state_code as state_code,
        _out_hiin_id as hiin_id ,
        'Hospital - ' :: VARCHAR || DENSE_RANK () OVER (ORDER BY _out_hiin_id) :: CHAR(3) as hospital_name,
        _out_time_frame as time_frame,
        _out_year_month as year_month,
        _out_rate as rate,
        _out_rep_hosp as rep_hosp
        from usp_get_hospital_hret_report($1,$2,$3,$4,$5,$6,$7,null,$8,$9)`
    }

    async generateReport(reportData) {
        console.log(" Hosp : " + reportData);
        var _hospitalInformationRepository = new hospitalInformationRepository();
        var hretReport = await super.generateReport(reportData)
        var hospital = await _hospitalInformationRepository.getDataById(reportData.hospitalId)

        hretReport.report_level = "Hospital"
        hretReport.report_name = "HRET-Hospital-" + hospital.hospital_name;
        return hretReport;
    }

    async execute(connection, reportParameters) {

        var nationalAverage = connection.query(this.nationalAverageQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange]);

        var stateAverage = connection.query(this.stateAverageQuery, [reportParameters.subMeasure,
        reportParameters.startDate,
        reportParameters.endDate,
        reportParameters.peerGroup,
        reportParameters.isRural,
        reportParameters.bedRange,
        reportParameters.stateCode]);

        var hospital = connection.query(this.hospitalQuery,
            [reportParameters.subMeasure,
            reportParameters.startDate,
            reportParameters.endDate,
            reportParameters.peerGroup,
            reportParameters.isRural,
            reportParameters.bedRange,
            reportParameters.stateCode,
            reportParameters.hospitalId]);

        var comparables = connection.query(this.comparablesQuery, [reportParameters.subMeasure,
        reportParameters.startDate,
        reportParameters.endDate,
        reportParameters.peerGroup,
        reportParameters.isRural,
        reportParameters.bedRange,
        reportParameters.stateCode,
        reportParameters.comparables,
        reportParameters.noOfRandomSamples]);

        nationalAverage.catch(ex => { throw ex })
        stateAverage.catch(ex => { throw ex })
        hospital.catch(ex => { throw ex })
        comparables.catch(ex => { throw ex })


        var [nationalAverageRow, stateAverageRow, hospitalRow, comparablesRows] = await Promise.all([nationalAverage, stateAverage, hospital, comparables])
        return nationalAverageRow.concat(stateAverageRow).concat(hospitalRow).concat(comparablesRows);
    }

}
