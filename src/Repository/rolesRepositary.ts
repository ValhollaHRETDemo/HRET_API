
import { getManager, EntityRepository, Repository } from "typeorm";
import { roles } from "../entity/roles";
import * as _ from "underscore";
@EntityRepository()
export class rolesRepository extends Repository<roles> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(roles);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find();

        return _.sortBy(data, 'role_rank');
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    // save data into database by id
    async saveData(data: roles) {

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }

    //delete the data by id
    async deleteData(data: roles) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}