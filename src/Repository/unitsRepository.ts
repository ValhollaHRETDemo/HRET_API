
import { getManager, EntityRepository, Repository } from "typeorm";
import { units } from "../entity/units";
import * as _ from "underscore";
@EntityRepository()
export class unitsRepository extends Repository<units> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(units);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ is_active: "1" });

        return _.sortBy(data, 'unit_name');
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    //Get Specific data from database by id
    async getDataByHospId(hospitalId: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.find({ is_active: "1", hospital_id: hospitalId });

        return _.sortBy(data, 'unit_name');
    }

    // save data into database by id
    async saveData(data: units) {

        // activate data on save.
        data.is_active = "1";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        await this._repository.save(state);

        return this._repository.findOne({ unit_code: data.unit_code });
    }

    //delete the data by id
    async deleteData(data: units) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}