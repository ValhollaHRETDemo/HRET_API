
import { getManager, EntityRepository, Repository } from "typeorm";
import { hs_hospitals } from "../entity/hs_hospitals";

@EntityRepository()
export class healthSystemAndHospitalRepository extends Repository<hs_hospitals> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(hs_hospitals);

    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find({ relations: ["health_systems", "hosp_information"] });
        // const data = await this._repository.find({ relations: ["states"] });

        return data;
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    //Get Specific data from database by id
    async getDataByHealthSystemId(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.find({ health_system_id: id });

        return data;
    }

    // save data into database by id
    async saveData(data: hs_hospitals) {

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }

    //delete the data by id
    async deleteData(data: hs_hospitals) {

        //soft deleting the data
        data.is_active = '0';

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}