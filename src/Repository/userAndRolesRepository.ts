
import { getManager, EntityRepository, Repository } from "typeorm";
import { user_roles } from "../entity/user_roles";

@EntityRepository()
export class userAndRolesRepository extends Repository<user_roles> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(user_roles);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find();

        return data;
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    //Get Specific data from database by id
    async getDataByUserId(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne({ is_active: 1, user_id: id });

        return data;
    }

    // save data into database by id
    async saveData(data: user_roles) {

        //creates the users into the repository instance
        const userRole = this._repository.create(data);

        //save (or) insert the data into the db
        await this._repository.save(userRole);

        return this._repository.findOne({ user_id: data.user_id });
    }

    //delete the data by id
    async deleteData(data: user_roles) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }

    //delete the data by id
    async deleteDataById(user_id) {

        var data = await this._repository.findOne({ user_id: user_id });

        //soft deleting the data
        data.is_active = "0";

        console.log(data.is_active);

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return await this._repository.save(state);
    }
}