
import { getManager, EntityRepository, Repository } from "typeorm";
import {  hosp_units } from "../entity/hosp_units";

@EntityRepository()
export class hospitalUnitsRepository extends Repository<hosp_units> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(hosp_units);
    }

    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data
        const data = await this._repository.find();

        return data;
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.findOne(id);

        return data;
    }

    //Get Specific data from database by id
    async getDataByHospitalId(hospitalId: number) {

        // Instanciates repository and gets specific data
        var data = this._repository.find({ hospital_id: hospitalId });
        
        return data;
    }

    // save data into database by id
    async saveData(data: hosp_units) {

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return await this._repository.save(state);
    }

      //delete the data by id
      async deleteData(data: hosp_units) {

        //soft deleting the data
        data.is_active = "0";

        //creates the users into the repository instance
        const state = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(state);
    }
}