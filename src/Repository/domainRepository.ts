
import { getManager, EntityRepository, Repository } from "typeorm";
import { domain_management } from "../entity/domain";
import * as _ from "underscore";

@EntityRepository()
export class DomainRepository extends Repository<domain_management> {

    _repository: any;

    constructor() {
        super();
        //repository instance
        this._repository = getManager().getRepository(domain_management);
    }



    //Get all data from the database
    async getData() {

        // Instanciates repository and gets all data

        const data = await this._repository.find({ is_active: "1" });

        //return data
        return data;
    }




    // save data into database by id
    async saveData(data: domain_management) {

        //creates the domain into the repository instance
        const domain = await this._repository.create(data)

        //save (or) insert the data into the db
        await this._repository.save(domain);

        return this._repository.findOne({ domain: data.domain });
    }


    //delete the data by id
    async deleteData(data: domain_management) {

        //soft deleting the data
        data.is_active = "0";

        //creates the domain into the repository instance
        const domain = this._repository.create(data);

        //save (or) insert the data into the db
        return this._repository.save(domain);
    }

    //Get Specific data from database by id
    async getDataById(id: number) {

        // Instanciates repository and gets specific data
        var data = await this._repository.findOne(id);

        //return data
        return data;
    }

    async validateDomain(domain: String) {

        var data = await this._repository.findOne({ domain: domain });

        return data;
    }



}
