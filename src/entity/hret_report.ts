export interface hret_report {
    rows: Array<any>;
    time_window_columns: Array<Column>;
    property_columns: Array<Column>;
    key_columns: Array<Column>;
    report_level: string;
    report_name: string;
    report_generated_time: Date;
    keys: Array<string>;
}

export interface Column {
    id: String,
    name: String,
    index: Number,
    isHidden: Boolean
}
