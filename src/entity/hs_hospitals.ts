
import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {health_systems} from "./health_systems";
import {hosp_information} from "./hosp_information";


@Entity("hs_hospitals",{schema:"public"})
@Index("uc_hs_hospitals_health_system_id_hospital_id",["health_system_","hospital_",],{unique:true})
export class hs_hospitals {

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"hs_hosp_id"
        })
    hs_hosp_id:number;
        

   
    @OneToOne(type=>health_systems, health_systems=>health_systems.hs_hospitals,{  nullable:false, })
    @JoinColumn({ name:'health_system_id'})
    health_system_:health_systems | null;


   
    @OneToOne(type=>hosp_information, hosp_information=>hosp_information.hs_hospitals,{  nullable:false, })
    @JoinColumn({ name:'hospital_id'})
    hospital_:hosp_information | null;


    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;

    @Column("integer")
    health_system_id:number;
        
    
    @Column("integer")
    hospital_id:number;
}

