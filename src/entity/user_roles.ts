import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { users } from "./users";
import { roles } from "./roles";
import { hosp_information } from "./hosp_information";
import { units } from "./units";
import { states } from "./states";


@Entity("user_roles", { schema: "public" })
@Index("uc_user_roles_user_id", ["user_",], { unique: true })
export class user_roles {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "user_role_id"
    })
    user_role_id: number;

    @Column("integer")
    user_id: number;

    @OneToOne(type => users, users => users.user_roles, { nullable: false, })
    @JoinColumn({ name: 'user_id' })
    user_: users | null;

    @Column("integer")
    role_id: number;

    @ManyToOne(type => roles, roles => roles.user_roless, { nullable: false, })
    @JoinColumn({ name: 'role_id' })
    role_: roles | null;

    @Column("integer")
    hospital_id: number;

    @ManyToOne(type => hosp_information, hosp_information => hosp_information.user_roless, {})
    @JoinColumn({ name: 'hospital_id' })
    hospital_: hosp_information | null;

    @Column("integer")
    unit_id: number;

    @ManyToOne(type => units, units => units.user_roless, {})
    @JoinColumn({ name: 'unit_id' })
    unit_: units | null;

    @Column("integer", {
        nullable: true,
        name: "state_id"
    })
    state_id: number | null;

    @ManyToOne(type => states, states => states.userroles, {})
    @JoinColumn({ name: 'state_id' })
    state_: states | null;

    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;

    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;

    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;

    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;

    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;

    @Column("integer", {
        nullable: true,
        name: "hs_id"
    })
    hs_id: number | null;

}
