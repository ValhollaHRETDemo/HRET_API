import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { states } from "./states";
import { mf_authentication } from "./mf_authentication";
import { user_roles } from "./user_roles";
import { usersessions } from "./usersessions";


@Entity("users", { schema: "public" })
@Index("uc_users_email_id", ["email_id",], { unique: true })
export class users {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "user_id"
    })
    user_id: number;

    @Column("character varying", {
        nullable: false,
        unique: true,
        length: 200,
        name: "email_id"
    })
    email_id: string;

    @Column("character varying", {
        nullable: false,
        length: 256,
        name: "user_pwd"
    })
    user_pwd: string;

    @Column("character varying", {
        nullable: false,
        length: 50,
        name: "first_name"
    })
    first_name: string;

    @Column("character varying", {
        nullable: false,
        length: 50,
        name: "last_name"
    })
    last_name: string;

    @Column("character varying", {
        nullable: false,
        length: 20,
        name: "phone"
    })
    phone: string;

    @Column("character varying", {
        nullable: false,
        name: "is_locked"
    })
    is_locked: string;

    @Column("date", {
        nullable: false,
        default: "(now() + '90 days'::interval)",
        name: "password_expiry_date"
    })
    password_expiry_date: string;

    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;

    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;

    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;

    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;

    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;

    @Column("character varying", {
        nullable: false,
        name: "failure_count"
    })
    failure_count: string;

    @Column("character varying", {
        nullable: false,
        name: "is_auto_password"
    })
    is_auto_password: string;

    @OneToMany(type => mf_authentication, mf_authentication => mf_authentication.user_)
    mf_authentications: mf_authentication[];

    @OneToOne(type => user_roles, user_roles => user_roles.user_)
    user_roles: user_roles | null;

    @OneToMany(type => usersessions, usersessions => usersessions.user_)
    usersessionss: usersessions[];

}

