import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";


@Entity("hosp_group",{schema:"public"})
export class hosp_group {

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"hosp_group_id"
        })
    hosp_group_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:250,
        name:"hosp_group"
        })
    hosp_group:string;
        

    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;
        
}
