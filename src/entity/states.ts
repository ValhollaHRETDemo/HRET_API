import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { hosp_information } from "./hosp_information";
import { users } from "./users";
import { health_systems } from "./health_systems";
import { user_roles } from "./user_roles";


@Entity("states", { schema: "public" })
@Index("uc_states_state_code", ["state_code",], { unique: true })
export class states {

    @Column("character", {
        nullable: false,
        unique: true,
        length: 2,
        name: "state_code"
    })
    state_code: string;

    @Column("character varying", {
        nullable: false,
        length: 150,
        name: "state_name"
    })
    state_name: string;

    @Column("character varying", {
        nullable: true,
        name: "is_hret"
    })
    is_hret: string | null;

    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;

    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;

    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;

    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;

    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "state_id"
    })
    state_id: number;

    @OneToMany(type => hosp_information, hosp_information => hosp_information.state_)
    hosp_informations: hosp_information[];

    @OneToMany(type => user_roles, user_roles => user_roles.state_)
    userroles: user_roles[];

    @OneToMany(type => health_systems, health_systems => health_systems.state_)
    health_systemss: health_systems[];

}

