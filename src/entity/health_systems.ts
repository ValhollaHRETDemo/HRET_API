import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { states } from "./states";
import { hs_hospitals } from "./hs_hospitals";


@Entity("health_systems", { schema: "public" })
export class health_systems {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "health_system_id"
    })
    health_system_id: number;


    @Column("character varying", {
        nullable: false,
        length: 250,
        name: "health_system_name"
    })
    health_system_name: string;


    @Column("character varying", {
        nullable: false,
        length: 25,
        name: "contact_person"
    })
    contact_person: string;


    @Column("character varying", {
        nullable: false,
        length: 200,
        name: "email_id"
    })
    email_id: string;


    @Column("character varying", {
        nullable: false,
        length: 20,
        name: "phone"
    })
    phone: string;


    @Column("character varying", {
        nullable: false,
        length: 150,
        name: "address_line_1"
    })
    address_line_1: string;


    @Column("character varying", {
        nullable: false,
        length: 150,
        name: "address_line_2"
    })
    address_line_2: string;


    @Column("character varying", {
        nullable: false,
        length: 100,
        name: "city"
    })
    city: string;


    @Column("integer", {
        nullable: false,
        name: "zip_code"
    })
    zip_code: number;



    @ManyToOne(type => states, states => states.health_systemss, { nullable: false, })
    @JoinColumn({ name: 'state_id' })
    state_: states | null;

    @Column()
    state_id: string;

    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;


    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;


    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;


    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;


    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;



    @OneToOne(type => hs_hospitals, hs_hospitals => hs_hospitals.health_system_id)
    hs_hospitals: hs_hospitals | null;



}

