import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {hosp_information} from "./hosp_information";
import {units} from "./units";


@Entity("hosp_units",{schema:"public"})
@Index("uc_hosp_units_hospital_id_unit_code",["hospital_","unit_",],{unique:true})
export class hosp_units {

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"hosp_unit_id"
        })
    hosp_unit_id:number;
        
    @Column()
    hospital_id: number;
   
    @OneToOne(type=>hosp_information, hosp_information=>hosp_information.hosp_units,{  nullable:false, })
    @JoinColumn({ name:'hospital_id'})
    hospital_:hosp_information | null;
 
    @Column()
    unit_id: number;
   
    @OneToOne(type=>units, units=>units.hosp_units,{  nullable:false, })
    @JoinColumn({ name:'unit_id'})
    unit_:units | null;


    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;
        
}
