import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {user_roles} from "./user_roles";
import {hosp_units} from "./hosp_units";


@Entity("units",{schema:"public"})
@Index("uc_units_unit_code",["unit_code",],{unique:true})
export class units {

    @Column("character varying",{ 
        nullable:false,
        unique: true,
        length:5,
        name:"unit_code"
        })
    unit_code:string;
        

    @Column("character varying",{ 
        nullable:false,
        length:250,
        name:"unit_name"
        })
    unit_name:string;
        
    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;
        

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"unit_id"
        })
    unit_id:number;
        

   
    @OneToMany(type=>user_roles, user_roles=>user_roles.unit_)
    user_roless:user_roles[];
    

   
    @OneToOne(type=>hosp_units, hosp_units=>hosp_units.unit_)
    hosp_units:hosp_units | null;

}
