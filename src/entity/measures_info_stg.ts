import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";


@Entity("measures_info_stg",{schema:"public"})
export class measures_info_stg {

    @Column("character varying",{ 
        nullable:true,
        length:25,
        primary:true,
        name:"hiin_id"
        })
    hiin_id:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:1,
        name:"active"
        })
    active:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:350,
        name:"hospital_name"
        })
    hospital_name:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:1,
        name:"cah"
        })
    cah:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"rural"
        })
    rural:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"bedrange"
        })
    bedrange:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"icu"
        })
    icu:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"principalpeergroup"
        })
    principalpeergroup:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup1"
        })
    peergroup1:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup2"
        })
    peergroup2:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup3"
        })
    peergroup3:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup4"
        })
    peergroup4:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup5"
        })
    peergroup5:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peergroup6"
        })
    peergroup6:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"hen2"
        })
    hen2:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:2,
        name:"state"
        })
    state:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"statepartner_cd"
        })
    statepartner_cd:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"hiin_measureid"
        })
    hiin_measureid:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"timeframe"
        })
    timeframe:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"measurementstartdate"
        })
    measurementstartdate:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"measurementenddate"
        })
    measurementenddate:string | null;
        

    @Column("numeric",{ 
        nullable:true,
        precision:15,
        scale:5,
        name:"num"
        })
    num:string | null;
        

    @Column("numeric",{ 
        nullable:true,
        precision:15,
        scale:5,
        name:"denom"
        })
    denom:string | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"baseflag"
        })
    baseflag:number | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"months"
        })
    months:number | null;
        
}
