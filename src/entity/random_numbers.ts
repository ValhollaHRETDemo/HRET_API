import { Column, Entity } from "typeorm";


@Entity("random_numbers", { schema: "public" })
export class random_numbers {

    @Column({ primary: true })
    random_id: number;

    @Column()
    random_number: string;
}
