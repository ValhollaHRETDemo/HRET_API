
import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { users } from "./users";


@Entity("mf_authentication", { schema: "public" })
export class mf_authentication {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "mf_authentication_id"
    })
    mf_authentication_id: number;

    @Column("integer")
    user_id: number;


    @ManyToOne(type => users, users => users.mf_authentications, { nullable: false, })
    @JoinColumn({ name: 'user_id' })
    user_: users | null;


    @Column("integer", {
        nullable: false,
        name: "auth_code"
    })
    auth_code: number;


    @Column("timestamp with time zone", {
        nullable: false,
        name: "expiry_time"
    })
    expiry_time: Date;


    @Column("character varying", {
        nullable: false,
        name: "is_success"
    })
    is_success: string;


    @Column("integer", {
        nullable: false,
        default: "0",
        name: "failure_count"
    })
    failure_count: number;

    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;

}

