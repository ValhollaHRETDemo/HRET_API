export interface ReportInputParameters {
    measure: Number,
    subMeasure: Number,
    startDate: Date,
    endDate: Date,
    reportLevel: string,
    peerGroup: string,
    isRural: string,
    bedRange: Number,
    stateCode: string,
    hospitalId: string,
    percentile: Number,
    noOfRandomSamples: Number
}