import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";


@Entity("bed_range", { schema: "public" })
@Index("uc_bed_range_bed_range", ["bed_range",], { unique: true })
export class bed_range {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "bed_range_id"
    })
    bed_range_id: number;


    @Column("character varying", {
        nullable: false,
        unique: true,
        length: 250,
        name: "bed_range"
    })
    bed_range: string;


    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;


    @Column()
    sort_by: number

    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;


    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;


    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;


    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;

}
