import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";


@Entity("sub_measures", { schema: "public" })
@Index("uc_measure_codes_measure_code", ["measure_code",], { unique: true })
export class sub_measures {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "sub_measure_id"
    })
    sub_measure_id: number;


    @Column("character varying", {
        nullable: false,
        unique: true,
        length: 30,
        name: "measure_code"
    })
    measure_code: string;


    @Column("character varying", {
        nullable: false,
        length: 250,
        name: "measure_description"
    })
    measure_description: string;


    @Column()
    is_active: string;


    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;


    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;


    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;


    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;


    @Column("date", {
        nullable: true,
        name: "baseline_start_period"
    })
    baseline_start_period: string | null;


    @Column("integer", {
        nullable: true,
        name: "measure_id"
    })
    measure_id: number | null;


    @Column("integer", {
        nullable: true,
        name: "mul_by"
    })
    mul_by: number | null;


    @Column("character varying", {
        nullable: true,
        length: 100,
        name: "rpt_table_name"
    })
    rpt_table_name: string | null;

}
