import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {user_roles} from "./user_roles";


@Entity("roles",{schema:"public"})
export class roles {

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"role_id"
        })
    role_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        length:100,
        name:"role_name"
        })
    role_name:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"role_rank"
        })
    role_rank:number;
        

    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;
        

   
    @OneToMany(type=>user_roles, user_roles=>user_roles.role_)
    user_roless:user_roles[];
    
}

