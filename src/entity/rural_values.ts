import { Column, Entity } from "typeorm";

@Entity("rural_values", { schema: "public" })
export class rural_values {

    @Column({ primary: true })
    rural_id: number;

    @Column()
    rural_value: string;

    @Column()
    display_name: string;

}
