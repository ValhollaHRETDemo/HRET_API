import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";
import {users} from "./users";


@Entity("usersessions",{schema:"public"})
export class usersessions {

   
    @ManyToOne(type=>users, users=>users.usersessionss,{  nullable:false, })
    @JoinColumn({ name:'user_id'})
    user_:users | null;

    @Column("integer")
    user_id:number;


    @Column("character varying",{ 
        nullable:false,
        length:255,
        primary:true,
        name:"session_id"
        })
    session_id:string;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        name:"start_time"
        })
    start_time:Date;
        

    @Column("timestamp without time zone",{ 
        nullable:false,
        name:"end_time"
        })
    end_time:Date;
        

    @Column("character varying",{ 
        nullable:false,
        length:512,
        name:"browser_meta_data"
        })
    browser_meta_data:string;
        

    @Column("character varying",{ 
        nullable:false,
        name:"is_expired"
        })
    is_expired:string;
        
}
