import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";


@Entity("rpt_gen_log", { schema: "public" })
export class rpt_gen_log {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "rpt_gen_id"
    })
    rpt_gen_id: number;

    @Column()
    measure_id: number;

    @Column()
    user_id: string;

    @Column()
    rpt_status: string;


    @Column()
    rpt_error: string;


    @Column()
    rpt_parameter: string;




    @Column()
    rpt_start_time: Date;


    @Column()
    rpt_end_time: Date;

}
