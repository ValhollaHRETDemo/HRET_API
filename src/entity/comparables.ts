import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";


@Entity("comparables", { schema: "public" })
export class comparables {

    @Column({ primary: true })
    comparable_id: number;


    @Column()
    comparable_name: string;

    @Column()
    comparable_value: number;


    @Column()
    is_active: string;


    @Column()
    created_on: Date;


    @Column()
    created_by: number;


    @Column()
    modified_on: Date;


    @Column()
    modified_by: number;

    @Column()
    sort_order: number;

}
