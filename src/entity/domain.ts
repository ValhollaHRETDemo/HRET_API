import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";


@Entity("domain_management",{schema:"public"})
@Index("uc_domain_management_domain",["domain",],{unique:true})
export class domain_management {

    @Column("integer",{ 
        generated:true,
        nullable:false,
        primary:true,
        name:"domain_id"
        })
    domain_id:number;
        

    @Column("character varying",{ 
        nullable:false,
        unique: true,
        length:250,
        name:"domain"
        })
    domain:string;

    @Column("character varying",{ 
        nullable:false,
        unique: true,
        length:250,
        name:"domain_desc"
        })
    domain_desc:string;
        

    @Column("character varying",{ 
        nullable:false,
        name:"is_active"
        })
    is_active:string;
        

    @Column("integer",{ 
        nullable:false,
        name:"created_by"
        })
    created_by:number;
        

    @Column("timestamp with time zone",{ 
        nullable:false,
        default:"now()",
        name:"created_on"
        })
    created_on:Date;
        

    @Column("integer",{ 
        nullable:true,
        name:"modified_by"
        })
    modified_by:number | null;
        

    @Column("timestamp with time zone",{ 
        nullable:true,
        name:"modified_on"
        })
    modified_on:Date | null;
        
}
