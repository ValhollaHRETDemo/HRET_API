// 

import { Index, Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId } from "typeorm";
import { states } from "./states";
import { hs_hospitals } from "./hs_hospitals";
import { user_roles } from "./user_roles";
import { hosp_units } from "./hosp_units";


@Entity("hosp_information", { schema: "public" })
export class hosp_information {

    @Column("integer", {
        generated: true,
        nullable: false,
        primary: true,
        name: "hospital_id"
    })
    hospital_id: number;


    @Column("character varying", {
        nullable: false,
        length: 250,
        name: "hospital_name"
    })
    hospital_name: string;


    @Column("character varying", {
        nullable: false,
        length: 100,
        name: "contact_person"
    })
    contact_person: string;


    @Column("character varying", {
        nullable: false,
        length: 200,
        name: "email_id"
    })
    email_id: string;


    @Column("character varying", {
        nullable: false,
        length: 20,
        name: "phone"
    })
    phone: string;


    @Column("character varying", {
        nullable: false,
        length: 150,
        name: "address_line_1"
    })
    address_line_1: string;


    @Column("character varying", {
        nullable: true,
        length: 150,
        name: "address_line_2"
    })
    address_line_2: string | null;


    @Column("character varying", {
        nullable: false,
        length: 100,
        name: "city"
    })
    city: string;


    @Column("integer", {
        nullable: false,
        name: "zip_code"
    })
    zip_code: number;


    @Column("character varying", {
        nullable: false,
        name: "is_active"
    })
    is_active: string;


    @Column("integer", {
        nullable: false,
        name: "created_by"
    })
    created_by: number;




    @Column("timestamp with time zone", {
        nullable: false,
        default: "now()",
        name: "created_on"
    })
    created_on: Date;


    @Column("integer", {
        nullable: true,
        name: "modified_by"
    })
    modified_by: number | null;


    @Column("timestamp with time zone", {
        nullable: true,
        name: "modified_on"
    })
    modified_on: Date | null;

    @Column("integer")
    state_id: number;

    @Column("integer", {
        nullable: true,
        name: "hosp_peer_group_id"
    })
    hosp_peer_group_id: number;

    @Column("integer", {
        nullable: true,
        name: "bed_range_id"
    })
    bed_range_id: number;

    @Column("integer", {
        nullable: true,
        name: "rural_id"
    })
    rural_id: number;

    @Column("character varying", {
        nullable: true,
        length: 150,
        name: "hiin_id"
    })
    hiin_id: string | null;

    @ManyToOne(type => states, states => states.hosp_informations, {})
    @JoinColumn({ name: 'state_id' })
    state_: states | null;



    @OneToOne(type => hs_hospitals, hs_hospitals => hs_hospitals.hospital_id)
    hs_hospitals: hs_hospitals | null;



    @OneToMany(type => user_roles, user_roles => user_roles.hospital_)
    user_roless: user_roles[];



    @OneToOne(type => hosp_units, hosp_units => hosp_units.hospital_)
    hosp_units: hosp_units | null;

}
