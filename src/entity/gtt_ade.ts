import {Index,Entity, PrimaryColumn, Column, OneToOne, OneToMany, ManyToOne, ManyToMany, JoinColumn, JoinTable, RelationId} from "typeorm";


@Entity("gtt_ade",{schema:"public"})
@Index("idx_test_gtt_ade_1_hiin_id",["hiin_id",])
export class gtt_ade {

    @Column("character varying",{ 
        nullable:true,
        length:25,
        primary:true,
        name:"hiin_id"
        })
    hiin_id:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:1,
        name:"active"
        })
    active:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:350,
        name:"hospital_name"
        })
    hospital_name:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:1,
        name:"cah"
        })
    cah:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"rural"
        })
    rural:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"bed_range"
        })
    bed_range:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"icu"
        })
    icu:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"principal_peer_group"
        })
    principal_peer_group:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_1"
        })
    peer_group_1:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_2"
        })
    peer_group_2:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_3"
        })
    peer_group_3:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_4"
        })
    peer_group_4:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_5"
        })
    peer_group_5:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"peer_group_6"
        })
    peer_group_6:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"hen_2"
        })
    hen_2:string | null;
        

    @Column("character",{ 
        nullable:true,
        length:2,
        name:"state_code"
        })
    state_code:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:30,
        name:"state_partner_cd"
        })
    state_partner_cd:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:250,
        name:"hiin_measure_id"
        })
    hiin_measure_id:string | null;
        

    @Column("character varying",{ 
        nullable:true,
        length:50,
        name:"time_frame"
        })
    time_frame:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"measurement_start_date"
        })
    measurement_start_date:string | null;
        

    @Column("date",{ 
        nullable:true,
        name:"measurement_end_date"
        })
    measurement_end_date:string | null;
        

    @Column("numeric",{ 
        nullable:true,
        precision:15,
        scale:5,
        name:"numerator"
        })
    numerator:string | null;
        

    @Column("numeric",{ 
        nullable:true,
        precision:15,
        scale:5,
        name:"denominator"
        })
    denominator:string | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"base_flag"
        })
    base_flag:number | null;
        

    @Column("integer",{ 
        nullable:true,
        name:"months"
        })
    months:number | null;
        
}
