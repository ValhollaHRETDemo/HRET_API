import * as nodemailer from "nodemailer";
import { Request, Response } from "express";
import { UserRepository } from "../Repository/userRepository";

export class Mailer {

    content: string;
    constructor() {

    }

    async sendMail(receiverName: string, _receiverEmail: string, mailType: string, code: string) {
        var _userRepository = new UserRepository();

        const autoPasswordMailContent = `<!DOCTYPE html> <html> <style> body {padding: 20px !important;font-family: Arial, Helvetica, sans-serif;} .container { padding: 20px !important; background-color: #f1f1f1; } center { padding: 5px; background-color: #f1f1f1; } .code { padding: 5px; color: #FFFFFF; background-color: #048afd; } .content{ min-height: 300px !important; } </style> <body> <div class="container"> <center><h1>HRET - Login Information</h1></center> <hr/> <div class="content"> <span><b>Hi ${receiverName},</b></span> <p>Given below is the temporary password for your login:</p> <h4>Temporary Password:<span class="code">${code}</span></h4> <p>Please do no share this password.</p> <font face="Courier New" color="Gray" size="2"><br> DISCLAIMER:<br> The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain confidential or privileged information. Before opening attachments please check them for viruses and defects. HRET will not be responsible for any viruses or defects or any forwarded attachments emanating either from within HRET or outside. Any unauthorized use or dissemination of this message in whole or in part is strictly prohibited. Please note that e-mails are susceptible to change and HRET shall not be liable for any improper, untimely or incomplete transmission.<br> </font> </div> <hr/> <center> <a  href="http://www.hret.org">www.hret.org</a> | 155 North Wacker | Suite 400 | Chicago, IL 60606 </center> </div> </body> </html>`;

        const mfaMailContent = `<!DOCTYPE html> <html> <style> body {padding: 20px !important;font-family: Arial, Helvetica, sans-serif;} .container { padding: 20px !important; background-color: #f1f1f1; } center { padding: 5px; background-color: #f1f1f1; } .code { padding: 5px; color: #FFFFFF; background-color: #048afd; } .content{ min-height: 300px !important; } </style> <body> <div class="container"> <center><h1>HRET - One Time Password</h1></center> <hr/> <div class="content"> <span><b>Hi ${receiverName},</b></span> <p>Given below is a one time authentication code for your login:</p> <h4>Authentication Code:<span class="code">${code}</span></h4> <p>This code will expire in 5 minutes.</p> <font face="Courier New" color="Gray" size="2"><br> DISCLAIMER:<br> The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain confidential or privileged information. Before opening attachments please check them for viruses and defects. HRET will not be responsible for any viruses or defects or any forwarded attachments emanating either from within HRET or outside. Any unauthorized use or dissemination of this message in whole or in part is strictly prohibited. Please note that e-mails are susceptible to change and HRET shall not be liable for any improper, untimely or incomplete transmission.<br> </font> </div> <hr/> <center> <a  href="http://www.hret.org">www.hret.org</a> | 155 North Wacker | Suite 400 | Chicago, IL 60606 </center> </div> </body> </html>`;

        switch (mailType) {
            case "MFA":
                this.content = mfaMailContent;
                break;

            case "AutoPassword":
                this.content = autoPasswordMailContent;
                break;

            default:
                this.content = autoPasswordMailContent;
                break;
        }

        // create reusable transporter object using the default SMTP transport
        let transporter = nodemailer.createTransport({
            host: 'mail.globalmantrai.com',
            port: 25,
            secure: false, // true for 465, false for other ports
            auth: {
                user: 'rvinoth@globalmantrai.com',
                pass: 'welcome@12'
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        // setup email data with unicode symbols
        let mailOptions = {
            from: '"HRET" <vinothkumar.ramasamy@globalmantrai.com>',
            to: _receiverEmail,
            subject: 'HRET Email',
            //text: 'Hello', 
            html: this.content
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
            console.log("mailer error:" + error);
            if (error) {
                return ({ status: false, msg: error });
            }

            return ({ status: true, msg: 'Email has been sent' });
        });
    }
}