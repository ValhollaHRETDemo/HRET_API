import { Request, Response } from "express";
import { healthSystemAndHospitalRepository } from "../Repository/healthSystemAndHospitalRepository";

//gets all the data from database
export async function gethealthSystemAndHospital(request: Request, response: Response) {

   try{
    var _healthSystemAndHospitalRepository = new healthSystemAndHospitalRepository();

    var data = await _healthSystemAndHospitalRepository.getData();

    response.status(200).send(data);
}

    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function gethealthSystemAndHospitalById(request: Request, response: Response) {
    
    try{
    var _healthSystemAndHospitalRepository = new healthSystemAndHospitalRepository();

    var data = await _healthSystemAndHospitalRepository.getDataById(request.params.id);

    response.status(200).send(data);
}

    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
        }
}

export async function getHealthSystemHospitalByHealthSystemId(request: Request, response: Response){
    try{

    var _healthSystemAndHospitalRepository = new healthSystemAndHospitalRepository();

    var data = await _healthSystemAndHospitalRepository.getDataByHealthSystemId(request.params.id);

    response.status(200).send(data);
    
}
catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function savehealthSystemAndHospital(request: Request, response: Response) {
    try{
    var _healthSystemAndHospitalRepository = new healthSystemAndHospitalRepository();

    request.body.forEach(element => {
        var data = _healthSystemAndHospitalRepository.saveData(element);
    });

    //var data = await _healthSystemAndHospitalRepository.saveData(request.body);

    response.status(500).send("");
}
catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deletehealthSystemAndHospital(request: Request, response: Response) {
    try{
    var _healthSystemAndHospitalRepository = new healthSystemAndHospitalRepository();

    var data = await _healthSystemAndHospitalRepository.deleteData(request.body);

    response.status(200).send(data);

}
catch(e)
    {
response.status(400)
.json({
    status:400,
    message: e.message
});
}
}