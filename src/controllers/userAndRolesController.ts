import { Request, Response } from "express";
import { userAndRolesRepository } from "../Repository/userAndRolesRepository";

//gets all the data from database
export async function getUserAndRoles(request: Request, response: Response) {

    try{
   
    var _userAndRolesRepository = new userAndRolesRepository();

    var data = await _userAndRolesRepository.getData();

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUserAndRolesById(request: Request, response: Response) {

    try{
    var _userAndRolesRepository = new userAndRolesRepository();

    var data = await _userAndRolesRepository.getDataById(request.params.id);

    response.status(200).send(data);

}
catch(e)
{
    response.status(400)
        .json({
            status:400,
            message: e.message
        });
}
}

//gets specific the data from database
export async function getUserAndRolesByUserId(request: Request, response: Response) {
    try{
    var _userAndRolesRepository = new userAndRolesRepository();

    var data = await _userAndRolesRepository.getDataByUserId(request.params.id);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveUserAndRoles(request: Request, response: Response) {

    try{
    var _userAndRolesRepository = new userAndRolesRepository();

    var data = await _userAndRolesRepository.saveData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteUserAndRoles(request: Request, response: Response) {

    try{
    var _userAndRolesRepository = new userAndRolesRepository();

    var data = await _userAndRolesRepository.deleteData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}