import { Request, Response } from "express";
import { DomainRepository } from "../Repository/domainRepository";

//gets all the data from database
export async function getDomain(request: Request, response: Response) {

    try {

        var _domainRepository = new DomainRepository();

        var data = await _domainRepository.getData();

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getDomainById(request: Request, response: Response) {

    try {
        var _domainRepository = new DomainRepository();

        var data = await _domainRepository.getDataById(request.body);

        response.status(200).send(data);
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//inserts (or) updates the data into database
export async function saveDomain(request: Request, response: Response) {
    try {
        var _domainRepository = new DomainRepository();

        var data = await _domainRepository.saveData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteDomain(request: Request, response: Response) {

    try {
        var _domainRepository = new DomainRepository();

        var data = await _domainRepository.deleteData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//validate if domain is present
export async function validateDomain(request: Request, response: Response) {

    try {
        var _domainRepository = new DomainRepository();

        var data = await _domainRepository.validateDomain(request.body.domain);

        if (data) {
            response.status(200).send({ status: true, message: "Domain is registered" });
        }
        else {
            response.status(200).send({ status: false, message: "Domain is not registered" });
        }
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}





