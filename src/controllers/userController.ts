import { Request, Response } from "express";
import { UserRepository } from "../Repository/userRepository";
import { rolesRepository } from "../Repository/rolesRepositary";
import { userAndRolesRepository } from "../Repository/userAndRolesRepository";
import { multifactorAuthenticationRepository } from "../Repository/multiFactorAuthenticationRepository";
import { Mailer } from "../helpers/mailHelper";
import { mf_authentication } from "../entity/mf_authentication";
import * as moment from 'moment';
import { StateRepository } from "../Repository/statesRepository";
import { hospitalInformationRepository } from "../Repository/hospitalInformationRepository";

// var salt = "sddsj4o3j4lljq3434n3nq.ewqe.,43";
// var AppConfig = require("../config")
// const md5 = require("md5");
// const UUID = require("uuid/v4");

var _mailer = new Mailer();
var _userRepository = new UserRepository();

//login
export async function login(request: Request, response: Response) {
    try {

        var _userRepository = new UserRepository();
        var _userAndRolesRepository = new userAndRolesRepository();
        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        const user = await _userRepository.getUserDetails(request.body.email_id.toLocaleLowerCase());

        if (user == undefined || user.length == 0) {
            response.status(401).json({ status: 401, message: "Account does not exist. Please enter a valid email." });
        }
        else {
            const userRole = await _userAndRolesRepository.getDataByUserId(user.user_id)
            if (userRole == undefined || userRole.length == 0) {
                response.status(401).json({ status: 401, message: "The account is not associated with any role. Please contact the administrator." });
            } else {
                if (user.user_pwd == request.body.user_pwd) {
                    if (user.is_locked == "0") {

                        //console.log("Date parse function output", Date.parse(user.password_expiry_date), "and", Date.parse(new Date().toDateString()));
                        if (Date.parse(user.password_expiry_date) < Date.parse(new Date().toDateString())) {
                            response.status(401).json({ status: 401, message: "Your password has been expired. Please contact the administrator." });
                        }
                        else { //Successful Login
                            //Auto password
                            if (user.is_auto_password === "1") {
                                response.status(200).json({
                                    is_auto_password: user.is_auto_password,
                                    user_id: user.user_id
                                });
                            }
                            //Custom password
                            else {
                                var code = _userRepository.generateRandomMFA();
                                var object = new mf_authentication();
                                object.user_id = user.user_id;
                                object.auth_code = code;
                                object.is_active = "1";
                                object.expiry_time = moment().add(5, 'minutes').toDate();
                                object.is_success = "0";
                                var mfData = await _multifactorAuthenticationRepository.saveData(object);
                                var mailResponse = await _mailer.sendMail((user.first_name + " " + user.last_name), user.email_id, "MFA", code);
                                response.status(200).json({
                                    is_auto_password: user.is_auto_password,
                                    user_id: user.user_id
                                });
                            }
                        }
                    }
                    else {
                        response.status(401).json({ status: 401, message: "Your account has been locked. Please contact the administrator." });
                    }
                }
                else {
                    response.status(401).json({ status: 401, message: "Incorrect password" });
                }
            }
        }
    } catch (e) {
        response.status(400).json({ errorMessage: e.message });
    }
}

//verification: MFA
export async function verification(request: Request, response: Response) {
    try {
        console.log("Verification");
        var _userRepository = new UserRepository();
        var _roleRepository = new rolesRepository();
        var _userRolesRepository = new userAndRolesRepository();
        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();
        var _statesRepository = new StateRepository();
        var _hospitalInformationRepository = new hospitalInformationRepository();
        var state_details = null;
        var hospital_details = null;

        var userDetails = await _userRepository.getDataById(request.body.user_id);

        if (userDetails.failure_count >= 5) {
            response.status(400).json({ status: 400, message: "Your account has been locked. Please contact the administrator." });
        }
        else {
            var authData = await _multifactorAuthenticationRepository.getLatestAuthDataByUserId(request.body.user_id);
            if (authData == undefined || authData == null) {
                updateMFAFailureCount(authData);
                updateUserFailureCount(authData);
                response.status(400).json({ status: 400, message: "Invalid attempt. Please try re-login." });
            }
            else if (authData.auth_code == request.body.auth_code && moment().isAfter(authData.expiry_time)) {
                //updateMFAFailureCount(authData);
                response.status(400).json({ result: false, message: "Verification code expired. Please try re-login." });
            }
            else if (authData.auth_code == request.body.auth_code) {
                authData.is_success = "1";

                var updateResponse = await _multifactorAuthenticationRepository.saveData(authData);
                const userRoleDetails = await _userRolesRepository.getDataByUserId(request.body.user_id);
                const roleDetails = await _roleRepository.getDataById(userRoleDetails.role_id);
                if (userRoleDetails.state_id) {
                    var state_data = await _statesRepository.getDataById(userRoleDetails.state_id);
                    state_details = (state_data) ? state_data : null;
                }
                if (userRoleDetails.hospital_id) {
                    var hospital_data = await _hospitalInformationRepository.getDataById(userRoleDetails.hospital_id);
                    hospital_details = (hospital_data) ? hospital_data : null;
                }
                const token = await _userRepository.generateToken(request.body.user_id, roleDetails, userRoleDetails, state_details, hospital_details);

                response.status(200).json({ result: true, message: "Verification successful.", token: token });
            }
            else {
                updateMFAFailureCount(authData);
                response.status(400).json({ result: false, message: "Incorrect verification code." });
            }
        }
    } catch (e) {

        response.status(400).json({ errorMessage: e.message });
    }
}

export async function generateOTP(request: Request, response: Response) {
    try {

        var _userRepository = new UserRepository();
        var user_id = request.body.user_id;
        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        var userDetails = await _userRepository.getDataById(user_id);

        if (userDetails) {
            const user = await _userRepository.getUserDetails(userDetails.email_id);
            var code = _userRepository.generateRandomMFA();
            var object = new mf_authentication();
            object.user_id = user_id;
            object.auth_code = code;
            object.is_active = "1";
            object.expiry_time = moment().add(5, 'minutes').toDate();
            object.is_success = "0";
            var mfData = await _multifactorAuthenticationRepository.saveData(object);
            var mailResponse = await _mailer.sendMail((user.first_name + " " + user.last_name), user.email_id, "MFA", code);
            console.log("success");
            response.status(200).json({
                is_auto_password: user.is_auto_password,
                user_id: user.user_id
            });
        } else {
            response.status(400).json({ status: 400, message: "Your account has been locked. Please contact the administrator." });
        }
    } catch (e) {
        response.status(400).json({ errorMessage: e.message });
    }
}

//gets all the data from database
export async function getUsers(request: Request, response: Response) {
    try {

        var _userRepository = new UserRepository();

        var data = await _userRepository.getData();

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUsersById(request: Request, response: Response) {
    try {
        var _userRepository = new UserRepository();

        var data = await _userRepository.getDataById(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUserLoginDetails(request: Request, response: Response) {
    try {

        var _userRepository = new UserRepository();

        var data = await _userRepository.getUserLoginDetails(request.body.emailId, request.body.password);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveUsers(request: Request, response: Response) {

    try {
        var _userRepository = new UserRepository();

        var data = await _userRepository.saveData(request.body);
        if (data.is_auto_password == '1') {
            var mailResponse = await _mailer.sendMail((data.first_name + " " + data.last_name), data.email_id, "AutoPassword", data.user_pwd);

        }
        response.status(200).send(data);
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteUsers(request: Request, response: Response) {

    try {
        var _userAndRolesRepository = new userAndRolesRepository();
        var userRoleDelete = await _userAndRolesRepository.deleteDataById(request.body.user_id);

        var _userRepository = new UserRepository();
        var userDelete = await _userRepository.deleteData(request.body);

        response.status(200).send(userDelete);
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUsersByUserId(request: Request, response: Response) {
    var _userRepository = new UserRepository();

    var data = await _userRepository.getData();

    response.send(data);
}

//gets specific the data from database
export async function getUserDetails(request: Request, response: Response) {
    var _userRepository = new UserRepository();

    var userDetails = await _userRepository.getUserDetails(request.body.email_id);

    response.send(userDetails);
}

// to change the password
export async function changePassword(request: Request, response: Response) {
    try {
        var _userRepository = new UserRepository();

        var userDetails = await _userRepository.getDataById(request.body.user_id);
        if (userDetails) {
            if (userDetails.user_pwd !== request.body.user_pwd) {
                response.status(400)
                    .json({
                        status: 400,
                        message: "Invalid password. Please try again."
                    });

            }
            else if (userDetails.user_pwd === request.body.new_pwd) {
                response.status(400)
                    .json({
                        status: 400,
                        message: "New password and old password cannot be same. Please try again."
                    });
            }
            else {
                userDetails.user_pwd = request.body.new_pwd;
                userDetails.is_auto_password = "0";
                userDetails.modified_on = moment();
                userDetails.modified_by = request.body.user_id;
                var data = await _userRepository.saveData(userDetails);
                response.status(200).send(data);
            }
        }
        else {
            response.status(400)
                .json({
                    status: 400,
                    message: "Invalid attempt. Please contact the administrator."
                });
        }
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

// to reset the password
export async function resetPassword(request: Request, response: Response) {

    try {

        var _userRepository = new UserRepository();

        var userDetails = await _userRepository.getUserDetails(request.body.email_id);
        if (userDetails) {
            userDetails.user_pwd = _userRepository.generateRandomPassword();
            userDetails.is_auto_password = '1';
            userDetails.is_locked = '1';
            userDetails.modified_by = userDetails.user_id;
            userDetails.modified_on = moment();

            var data = await _userRepository.save(userDetails);
            if (data.is_auto_password == '1') {
                var mailResponse = await _mailer.sendMail((data.first_name + " " + data.last_name), data.email_id, "AutoPassword", data.is_auto_password);
            }
            response.status(200).send(data);
        }
        else {
            response.status(400)
                .json({
                    status: 400,
                    message: "Invalid user. Please try again."
                });
        }
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

async function updateMFAFailureCount(authData) {

    var _userRepository = new UserRepository();

    var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();
    authData.failure_count = authData.failure_count + 1;
    var updateResponse = await _multifactorAuthenticationRepository.saveData(authData);
    return updateResponse;
}

async function updateUserFailureCount(authData) {
    var _userRepository = new UserRepository();
    var userDetails = await _userRepository.getDataById(authData.user_id);
    userDetails.modified_on = moment();
    userDetails.modified_by = userDetails.user_id;
    userDetails.failure_count = authData.failure_count;
    userDetails.is_locked = (authData.failure_count >= 5) ? "1" : "0";
    var userUpdateResponse = await _userRepository.saveData(userDetails);

}