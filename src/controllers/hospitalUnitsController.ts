import { Request, Response } from "express";
import { hospitalUnitsRepository } from "../Repository/hostpitalUnitsRepository";

//gets all the data from database
export async function gethospitalUnits(request: Request, response: Response) {

    try{

    var _hospitalUnitsRepository = new hospitalUnitsRepository();

    var data = await _hospitalUnitsRepository.getData();

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function gethospitalUnitsById(request: Request, response: Response) {

    try{

    var _hospitalUnitsRepository = new hospitalUnitsRepository();

    var data = await _hospitalUnitsRepository.getDataById(request.params.id);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getDataByHospitalId(request: Request, response: Response) {

    try{
    var _hospitalUnitsRepository = new hospitalUnitsRepository();

    var data = await _hospitalUnitsRepository.getDataByHospitalId(request.params.id);

    response.status(200).send(data);

}
catch(e)
{
    response.status(400)
        .json({
            status:400,
            message: e.message
        });
}
}

//inserts (or) updates the data into database
export async function savehospitalUnits(request: Request, response: Response) {
   try{
    var _hospitalUnitsRepository = new hospitalUnitsRepository();
    
    request.body.forEach(element => {
        var data = _hospitalUnitsRepository.saveData(element);
    });
    //var data = await _hospitalUnitsRepository.saveData(request.body);

    response.status(500).send("");

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//delete a specific data in database
export async function deletehospitalUnits(request: Request, response: Response) {
    try{
    var _hospitalUnitsRepository = new hospitalUnitsRepository();

    var data = await _hospitalUnitsRepository.deleteData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}