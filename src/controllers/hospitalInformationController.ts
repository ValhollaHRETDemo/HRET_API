import { Request, Response } from "express";
import { hospitalInformationRepository } from "../Repository/hospitalInformationRepository";
import * as _ from "underscore";

//gets all the data from database
export async function getHospitalInformation(request: Request, response: Response) {

    try {

        var _hospitalInformationRepository = new hospitalInformationRepository();

        var data = await _hospitalInformationRepository.getData();

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getHospitalInformationById(request: Request, response: Response) {

    try {

        var _hospitalInformationRepository = new hospitalInformationRepository();

        var data = await _hospitalInformationRepository.getDataById(request.params.id);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getHospitalInformationByStateId(request: Request, response: Response) {

    try {

        var _hospitalInformationRepository = new hospitalInformationRepository();

        var data = await _hospitalInformationRepository.getDataByStateId(request.params.id);

        response.status(200).send(_.sortBy(data, 'hospital_name'));

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveHospitalInformation(request: Request, response: Response) {

    try {
        var _hospitalInformationRepository = new hospitalInformationRepository();

        var data = await _hospitalInformationRepository.saveData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteHospitalInformation(request: Request, response: Response) {
    try {
        var _hospitalInformationRepository = new hospitalInformationRepository();

        var data = await _hospitalInformationRepository.deleteData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}