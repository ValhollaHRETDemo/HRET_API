import { Request, Response } from "express";
import { healthSystemsRepository } from "../Repository/healthSystemsRepository";
import * as _ from "underscore";

//gets all the data from database
export async function gethealthSystems(request: Request, response: Response) {

    try {
        var _healthSystemsRepository = new healthSystemsRepository();

        var data = await _healthSystemsRepository.getData();

        response.status(200).send(data);
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function gethealthSystemsById(request: Request, response: Response) {

    try {

        var _healthSystemsRepository = new healthSystemsRepository();

        var data = await _healthSystemsRepository.getDataById(request.params.id);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function gethealthSystemsByStateId(request: Request, response: Response) {

    try {

        var _healthSystemsRepository = new healthSystemsRepository();

        var data = await _healthSystemsRepository.getDataByStateId(request.params.id);

        response.status(200).send(_.sortBy(data, 'health_system_name'));

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function savehealthSystems(request: Request, response: Response) {
    try {
        var _healthSystemsRepository = new healthSystemsRepository();

        var data = await _healthSystemsRepository.saveData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deletehealthSystems(request: Request, response: Response) {

    try {
        var _healthSystemsRepository = new healthSystemsRepository();

        var data = await _healthSystemsRepository.deleteData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}