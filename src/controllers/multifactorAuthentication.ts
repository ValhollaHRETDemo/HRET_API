import { Request, Response } from "express";
import { multifactorAuthenticationRepository } from "../Repository/multiFactorAuthenticationRepository";

//gets all the data from database
export async function getMultifactorAuthentication(request: Request, response: Response) {

    try {

        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        var data = await _multifactorAuthenticationRepository.getData();

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getMultifactorAuthenticationById(request: Request, response: Response) {

    try {

        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        var data = await _multifactorAuthenticationRepository.getDataById(request.params.id);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveMultifactorAuthentication(request: Request, response: Response) {
    try {
        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        var data = await _multifactorAuthenticationRepository.saveData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteMultifactorAuthentication(request: Request, response: Response) {
    try {
        var _multifactorAuthenticationRepository = new multifactorAuthenticationRepository();

        var data = await _multifactorAuthenticationRepository.deleteData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}