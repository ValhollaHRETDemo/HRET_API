import { Request, Response } from "express";
import { ReportsFilterRepository } from "../Repository/reportsFilterRepository";

//gets all the data from database
export async function getReportsFilter(request: Request, response: Response) {

    try {

        var reportsFilterRepository = new ReportsFilterRepository();

        var data = await reportsFilterRepository.getFilterData(request.params.id, request.query.level, request.query.hospital);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}
