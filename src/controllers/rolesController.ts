import { Request, Response } from "express";
import { rolesRepository } from "../Repository/rolesRepositary";

//gets all the data from database
export async function getRoles(request: Request, response: Response) {

    try{
   
    var _rolesRepository = new rolesRepository();

    var data = await _rolesRepository.getData();

    response.status(200).send(data);

}
catch(e)
{
    response.status(400)
        .json({
            status:400,
            message: e.message
        });
}
}

//gets specific the data from database
export async function getRolesById(request: Request, response: Response) {

    try{
    
    var _rolesRepository = new rolesRepository();

    var data = await _rolesRepository.getDataById(request.params.id);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveRoles(request: Request, response: Response) {

    try{
    var _rolesRepository = new rolesRepository();

    var data = await _rolesRepository.saveData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteRoles(request: Request, response: Response) {
    try{
    var _rolesRepository = new rolesRepository();

    var data = await _rolesRepository.deleteData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}