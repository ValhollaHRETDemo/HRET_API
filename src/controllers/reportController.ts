import { Request, Response } from "express";
import { ReportsRepository } from "../Repository/reportRepository";
import * as moment from "moment";
import { ReportFileGenerator } from "../Repository/ReportQueries/reportFileGenerator";
import * as xl from 'excel4node';

export async function getReports(request: Request, response: Response) {


    try {

        var _reportRepository = new ReportsRepository();

        var data = await _reportRepository.generateReport(
            {
                userId: request.body.param_user_id,
                measure: request.body.param_measure_id,
                subMeasure: request.body.param_sub_measure_id,
                startDate: request.body.param_start_date,
                endDate: request.body.param_end_date,
                reportLevel: request.body.param_report_level,
                peerGroup: request.body.param_peer_group,
                isRural: request.body.param_is_rural,
                bedRange: request.body.param_bed_range,
                stateCode: request.body.param_state_code,
                hospitalId: request.body.param_hospital_id,
                comparables: request.body.param_comparable,
                noOfRandomSamples: request.body.param_no_of_random_samples
            }
        );
        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

export async function downloadReportFile(request: Request, response: Response) {

    try {
        var reportFileGenerator = new ReportFileGenerator();
        var wb: xl.Workbook = await reportFileGenerator.generateFile(request.body.hret_report, request.body.user_filter_values);
        var fileName = request.body.hret_report.report_name + "_" + moment().format("YYYY-MM-DD") + ".xslx";

        wb.write(fileName, response)
    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }

}
