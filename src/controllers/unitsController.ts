import { Request, Response } from "express";
import { unitsRepository } from "../Repository/unitsRepository";

//gets all the data from database
export async function getUnits(request: Request, response: Response) {

    try {

        var _statesRepository = new unitsRepository();

        var data = await _statesRepository.getData();

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUnitsById(request: Request, response: Response) {

    try {
        var _statesRepository = new unitsRepository();

        var data = await _statesRepository.getDataById(request.params.id);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//gets specific the data from database
export async function getUnitsByHospId(request: Request, response: Response) {

    try {
        var _statesRepository = new unitsRepository();

        var data = await _statesRepository.getDataByHospId(request.params.id);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}

//inserts (or) updates the data into database
export async function saveUnits(request: Request, response: Response) {
    try {

        var _statesRepository = new unitsRepository();

        var data = await _statesRepository.saveData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}


//delete a specific data in database
export async function deleteUnits(request: Request, response: Response) {

    try {
        var _statesRepository = new unitsRepository();

        var data = await _statesRepository.deleteData(request.body);

        response.status(200).send(data);

    }
    catch (e) {
        response.status(400)
            .json({
                status: 400,
                message: e.message
            });
    }
}