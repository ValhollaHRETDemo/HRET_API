import { Request, Response } from "express";
import { StateRepository } from "../Repository/statesRepository";

//gets all the data from database
export async function getstates(request: Request, response: Response) {

    
try{
    var _statesRepository = new StateRepository();
    var data = await _statesRepository.getData();

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}

//get all the states which are associated to HRET.
export async function getAllData(request: Request, response: Response) {

    try{
    var _statesRepository = new StateRepository();
    var data = await _statesRepository.getAllStatesData();
    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}


//gets specific the data from database
export async function getstateById(request: Request, response: Response) {

    try{
    var _statesRepository = new StateRepository();

    var data = await _statesRepository.getDataById(request.params.id);

    response.status(200).send(data);

}
catch(e)
{
    response.status(400)
        .json({
            status:400,
            message: e.message
        });
}
}

//inserts (or) updates the data into database
export async function savestate(request: Request, response: Response) {
    try{
    var _statesRepository = new StateRepository();
    var data = await _statesRepository.saveData(request.body);

    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }

}


//delete a specific data in database
export async function deletestate(request: Request, response: Response) {
    try{
    var _statesRepository = new StateRepository();
    var data = await _statesRepository.deleteData(request.body);
    response.status(200).send(data);

    }
    catch(e)
    {
        response.status(400)
            .json({
				status:400,
                message: e.message
            });
    }
}