import "reflect-metadata";
import { createConnection } from "typeorm";
import { Request, Response } from "express";
import * as express from "express";
import * as bodyParser from "body-parser";
import { AppRoutes } from './routes/routes';





createConnection().then(async connection => {
    // create express app
    const app = express();
    app.use(bodyParser.json());

    // app.use(cors({origin: 'http://localhost:4200'}));
    var baseApi = process.env.BASE_API || "";
    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "*");
        res.header("Access-Control-Allow-Headers", "*, Origin, Content-Type, Accept, Authorization");
        next();
    });


    // register all application routes
    AppRoutes.forEach(route => {
        app[route.method](baseApi + route.path, (request: Request, response: Response, next: Function) => {
            route.action(request, response).then(() => next).catch(err => next(err));
        });
    });
    // run app
    app.listen(3000);
    console.log("API started successfully");
}).catch(error => console.log(error));


